package nl.utwente.di.temperature;

public class TemperatureConvertor {

    public static double celsiusToFahrenheit(Double celsius) {
        return (celsius * 1.8) + 32;
    }
}
